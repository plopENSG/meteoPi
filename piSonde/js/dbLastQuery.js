const influx = require('../js/dbConnect.js');

const sensors = ["press","temp","wind_min","wind_max","wind_mean","hygro","lum","wind_dir","rain","lat","lng"];


function getLastJSON(sensor) {
  let final_result = {"id": "011",
                      "name": "SONDE BMW"};

  if (sensor == 'all') {
    let promises = [];
    final_result["measurements"] = {};
    final_result["location"] = {};
    // promises.push(influx.query("select * from measures order by"))
    sensors.forEach(function(elem) {
      promises.push(queryLast(elem));
    });
    return Promise.all(promises).then(jsons => {
      jsons.forEach(function(json) {
        Object.keys(json).forEach (function(key) {
          if (key == "rain") {
            final_result["rain"] = json["time"];
          } else if (["lat","lng"].includes(key)) {
            final_result["location"][key] = json[key];
            final_result["location"]["date"] = json["time"];
          } else if (sensors.includes(key)) {
            final_result["measurements"][key] = json[key];
            final_result["measurements"]["date"] = json["time"];
          }
        });
      });
    }).then(result => final_result);

  } else if (sensor=="location") {
    final_result["location"] = {};
    let queries = [];
    queries.push(queryLast("lat").then(result => {
      final_result["location"]["lat"] = result["lat"];
      final_result["location"]["date"] = result["time"];
    }));
    queries.push(queryLast("lng").then(result => {
      final_result["location"]["lng"] = result["lng"];
    }));
    return Promise.all(queries).then(result => final_result);
  } else if (sensor=="rain") {
    return queryLast("rain").then(result => {
      final_result["rain"] = result["time"];
    }).then(result => final_result);
  } else if (sensors.includes(sensor)) {
    final_result["measurements"] = {};
    return queryLast(sensor).then(result => {
      final_result["measurements"][sensor] = result[sensor];
      final_result["measurements"]["date"] = result["time"];
    }).then(result => final_result);
  }

}
function queryLast(sensor) {
  if (sensor=="rain") {
    return influx.query('select last(value) as rain from rain where time > now() - 1m').then(result => result[0]);
  } else if (sensors.includes(sensor)) {
    return influx.query('select last(' + sensor + ') as ' + sensor + ' from measures where time > now() - 1m').then(result => result[0]);
  }
}
module.exports=getLastJSON;
