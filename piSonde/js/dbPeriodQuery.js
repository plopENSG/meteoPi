const influx = require('../js/dbConnect.js');

const sensors = ["press","temp","wind_min","wind_max","wind_mean","hygro","lum","wind_dir","rain"];


function getPeriodJSON(sensor,dateStart,dateEnd) {
  let final_result = {"id": "011",
                      "name": "SONDE BMW"};
  if (sensor == 'all') {
    let promises = [];
    final_result["data"] = [];
    final_result["rain"] = [];
    promises.push(influx.query("select * from measures where time > " + dateStart + " and time < " + dateEnd));
    promises.push(influx.query("select * from rain where time > " + dateStart + " and time < " + dateEnd));
    return Promise.all(promises).then(data => {
      for (let i = 0; i < data[0].length; i++) {
        let json = {"measurements": {},
                    "location": {}};
        sensors.forEach(function(sensor) {
          json["measurements"][sensor] = data[0][i][sensor];
        });
        json["location"]["lat"] = data[0][i]["lat"];
        json["location"]["lng"] = data[0][i]["lng"];
        json["location"]["date"] = data[0][i]["time"];
        json["measurements"]["date"] = data[0][i]["time"];
        final_result["data"].push(json);
      }
      for (let i = 0; i < data[1].length; i++) {
        final_result["rain"].push(data[1][i]["time"]);
      }

    }).then(result => final_result);

  } else if (sensor=="location") {
    final_result["data"] = [];
    return influx.query("select lat,lng from measures where time > " + dateStart + " and time < " + dateEnd).then(list_data => {
      for (let i = 0; i < list_data.length; i++) {
        let location = {"location": {}};
        location["location"]["lat"] = list_data[i]["lat"];
        location["location"]["lng"] = list_data[i]["lng"];
        location["location"]["date"] = list_data[i]["time"];
        final_result["data"].push(location);
      }
    }).then(result => final_result);
  } else if (sensor=="rain") {
    final_result["rain"] = [];
    return influx.query("select * from rain where time > " + dateStart + " and time < " + dateEnd).then(list_rain => {
      for (let i = 0; i < list_rain.length; i++) {
        final_result["rain"].push(list_rain[i]["time"]);
      }
    }).then(result => final_result);
  } else if (sensors.includes(sensor)) {
    final_result["data"] = [];
    return influx.query("select " + sensor + " from measures where time > " + dateStart + " and time < " + dateEnd).then(list_data => {
      for (let i = 0; i < list_data.length; i++) {
        let measurements = {"measurements": {}};
        measurements["measurements"][sensor] = list_data[i][sensor];
        measurements["measurements"]["date"] = list_data[i]["time"];
        final_result["data"].push(measurements);
      }
    }).then(result => final_result);
  }
}

module.exports=getPeriodJSON;
