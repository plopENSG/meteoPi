const Influx = require('influx');

const influx = new Influx.InfluxDB({
  host: 'localhost',
  database: 'meteopi',
  schema: [
    {
      measurement: 'measures',
      fields: {
        press: Influx.FieldType.FLOAT,
        temp: Influx.FieldType.FLOAT,
        hygro: Influx.FieldType.FLOAT,
        lum: Influx.FieldType.FLOAT,
        wind_mean: Influx.FieldType.FLOAT,
        wind_dir: Influx.FieldType.FLOAT,
        wind_min: Influx.FieldType.FLOAT,
        wind_max: Influx.FieldType.FLOAT,
        lat: Influx.FieldType.FLOAT,
        lng: Influx.FieldType.FLOAT
      },
      tags: []
    },
    {
      measurement: 'rain',
      fields: {
        value: Influx.FieldType.INTEGER
      },
      tags: []
    },
  ]
})

module.exports = influx;
