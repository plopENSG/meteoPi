
const influx = require('../js/dbConnect.js');
var Promise = require('bluebird')
var fs = Promise.promisifyAll(require('fs'));

var date = new Date().getMilliseconds();
var count = 0;
var count_rain = 0;
var path = '/dev/shm/';
function readFile() {

    var d = new Date().getMilliseconds();
    if (d > date) {
        count += d - date;
        count_rain += d - date;
    }
    else{
        count += 1000 + d - date;
        count_rain += 1000 + d - date;
    }
    date = d;
    if(count_rain > 1000) {
      fs.readFile(path + 'rainCounter.log', 'utf8', function(err, data) {
        influx.writePoints([
              {
                measurement: "rain",
                tags: {
                },
                fields: {value: 1},
                timestamp: new Date(data.replace(/\n|\r/g,'')),
              }
            ], {
              database: 'meteopi',
              precision: 's',
            });
      });
      count_rain -= 1000;
    }
    if(count > 60000) {
        let current_measure = {};
        let current_date = '';
        let promises = [];
        promises.push(fs.readFileAsync(path + 'sensors').then(function(result){
          let json = JSON.parse(result);
          current_measure.lum = parseFloat(json.measure[3].value);
          current_measure.wind_dir = Math.round(parseFloat(json.measure[4].value)*100)/100;
          current_measure.wind_mean = parseFloat(json.measure[5].value);
          current_measure.wind_max = parseFloat(json.measure[6].value);
          current_measure.wind_min = parseFloat(json.measure[7].value);
        }));
        promises.push(fs.readFileAsync(path + 'tph.log').then(function(result){
          let json = JSON.parse(result);
          current_date = json.date;
          current_measure.temp = parseFloat(json.temp);
          current_measure.hygro = parseFloat(json.hygro);
          current_measure.press = parseFloat(json.press);
        }));
        promises.push(fs.readFileAsync(path + 'gpsNmea').then(function(result){
          let data = String(result);
          let gpgga = data.split('\n')[0].split(",");
          current_measure.lat = parseFloat(gpgga[2])/100;
          current_measure.lng = parseFloat(gpgga[4])/100;
        }));

        Promise.all(promises).then(result => {
          influx.writePoints([
                {
                  measurement: "measures",
                  tags: {
                  },
                  fields: { press: current_measure["press"],
                            temp: current_measure["temp"],
                            hygro: current_measure["hygro"],
                            wind_min: current_measure["wind_min"],
                            wind_max: current_measure["wind_max"],
                            wind_dir: current_measure["wind_dir"],
                            wind_mean: current_measure["wind_mean"],
                            lum: current_measure["lum"],
                            lat: current_measure["lat"],
                            lng: current_measure["lng"],
                            },
                  timestamp: new Date(current_date),
                }
              ], {
                database: 'meteopi',
                precision: 's',
              });
        });
        count -= 60000;

    }
    setTimeout(readFile, 200);
}

readFile();
