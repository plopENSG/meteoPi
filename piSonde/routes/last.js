var express = require('express');
var router = express.Router();
const getJSON = require('../js/dbLastQuery.js');

router.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

/* GET home page. */
router.get('/', function(req, res, next) {
  let sensor = req.query.capteur_type;
  getJSON(sensor).then(result => {res.json(result)});
});

module.exports = router;
