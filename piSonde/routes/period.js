var express = require('express');
var router = express.Router();
const getJSON = require('../js/dbPeriodQuery.js');

router.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

/* GET users listing. */
router.get('/', function(req, res, next) {
  let sensor = req.query.capteur_type;

  let dateStart  = parseInt(req.query.dateStart,10) * 1000000000;
  let dateEnd = parseInt(req.query.dateEnd,10) * 1000000000;
  console.log(sensor,dateStart,dateEnd);
  getJSON(sensor,dateStart,dateEnd).then(result => {res.json(result)});
});

module.exports = router;
