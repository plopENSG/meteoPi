# Projet MeteoPi !

Il s'agit durant ce projet de construire un **serveur météo** (hébergé sur un de nos ordinateur) pouvant se connecter à plusieurs **stations météo**, celles-ci devant elles aussi être développées et déployées sur des  **RaspberryPi**. 
Il faut pour cela mettre en place une base de données qui s'actualise régulièrement sur la RaspberryPi, permettre les connections à cette dernière depuis l’extérieur et enfin développer l'interface utilisateur du serveur et ses requêtes aux RaspberryPis.

Pour lancer le serveur central, clonez le projet, placez vous dans le dossier piCentrale (piSonde est le code source de la sonde qui tourne sur le RaspberryPi) et installez les dépendances :

```bash
git clone https://gitlab.com/plopENSG/meteoPi.git
cd meteoPi/piCentrale
npm install
npm run serve
```
Le port par défault est 8080
Et connectez-vous sur [localhost:8080](http://localhost:8080) (le port par défaut est 8080).

# Partie RaspberryPi  

Il y a deux étapes principales dans cette partie, la construction et l'actualisation de la base de donnée (**InfluxDb**) et le développement du serveur d’accès à cette base de données.


## Création de la base de données

Nous utilisons ici InfluxDb qui est particulièrement bien adapter au stockage de mesures temporelles.
Les raspberryPi produisent quatre fichiers de données sur les indicateurs météorologiques, ces derniers sont mis à jour chaque seconde. Nous créons donc une base de donnée influx  **meteopi** et deux tables, une table **measures** qui contient tous les indicateurs météorologiques et la position, et une table  **rain**. 
Cette dernière est mis à part du fait de l’irrégularité des données, elle contient chaque moment de remplissage d'un godet de 2 ml de volume.

Enregistrer une entrée de la table **measures** toutes les secondes aboutirait à une base de donnée inutilement trop complexe, nous nous contentons donc de une entrée par minute. En revanche, la table **rain** est mise a jour toutes les secondes.
Le programme  mettant à jour cette base de donnée est codé en **node.js** et est lancé en **daemon** sur le RaspberryPi.


## Accès à la base de données

Maintenant que cette base de donnée est complétée et mise à jour il faut pourvoir faire des requêtes depuis un autre appareil que le raspberryPi.
Pour ce faire nous mettons en place un serveur **node.js** établissant un connexion à la base de donnée **Influx** à chaque requêtes.
Ces requêtes peuvent être de deux types :
* Une requête sur les dernières valeurs de un ou tous les indicateurs météorologiques avec comme paramètre de *Query* le type d'indicateur voulu (press: pression, temp: température, all : tous les indicateurs, etc...) Son adresse est du type :
> ipRaspberry:3001/last/?type_capteur=all
* Une requête sur les  valeurs de un ou tous les indicateurs météorologiques sur une durée voulue. Elle prends comme paramètres de *Query* le type d'indicateur voulu et un **timestamp** de début et de fin en seconde. Son adresse est du type :
> ipRaspberry:3001/period/?type_capteur=all&dateStart=1548524000&dateEnd=1548535000

Les résultats de ces requêtes sont renvoyés au format JSON de la forme suivante pour les requêtes **last** *(type_capteur = all)*  :


```javascript
var data = {
"id":"011",
"name":"SONDE BMW",
"measurements":{
	 "press":981.599,
	 "date":"2019-01-31T15:34:45.000Z",
	 "temp":28.252,
	 "wind_min":26.2,
	 "wind_max":60.5,
	 "wind_mean":40.7,
	 "hygro":30.2,
	 "lum":1,
	 "wind_dir":189.35
	 },
"location":{
	"lat":48.07035,
	"date":"2019-01-31T15:34:45.000Z",
	"lng":11.31311
	},
"rain":"2019-01-31T15:35:35.000Z"
}

```

Les résultats des requêtes **period** sont de cette forme *(type_capteur = all)*:


```javascript
var data = {
"id":"011",
"name":"SONDE BMW",
"data": [
	{"measurements":{
		"press":981.599,
		 "date":"2019-01-31T15:34:45.000Z",
		 "temp":28.252,
		 "wind_min":26.2,
		 "wind_max":60.5,
		 "wind_mean":40.7,
		 "hygro":30.2,
		 "lum":1,
		 "wind_dir":189.35
		 },
	"location":{
		"lat":48.07035,
		"date":"2019-01-31T15:34:45.000Z",
		"lng":11.31311
		}
	},
	{...},
	...]
"rain":[
	"2019-01-31T15:35:45.000Z",
	"2019-01-31T15:38:35.000Z",
	"2019-01-31T15:39:26.000Z",
	...]
}

```



# Partie interface
Nous avons utilisé **VueJS** pour développer l'interface, ainsi que **VueX** pour stocker les données. Nous avons aussi utilisé **Chartjs-Vue** pour les graphiques. Enfin, nous avons utilisé **Bootstrap-Vue** pour le CSS car nous avons l'habitude d'utiliser ce framework.

Nous avons mis en place 4 composants principaux correspondants aux 4 pages de l'interface:
- **Dashboard**, qui permet d'afficher toutes les dernières données enregistrées par une sonde, à choisir. Un composant **Measure** est utilisé pour gérer l'affichage de chaque donnée.
- **History**, qui permet d'afficher sous forme de graphique l'historique de chaque donnée d'une sonde, à choisir, sur une période, à choisir. Un composant **ChartHistory**, qui appelle lui-même un composant **Chart** pour chaque donnée.
- **Compare**, qui permet d'afficher une comparaison entre toutes les sonde d'une donnée, à choisir, sur une période ou de façon instantanée, à choisir.
- **Map**, qui affiche la position de chaque sonde sur une carte **MapBox**.

La prise en main de vueX nous a été un vrai challenge, mais très formateur.

@copyright Benoit Messiaen, Matthieu Peregrini, Witold Podlejski, ENSG - Ecole Nationale des Sciences Géographiques / National School of Geographical Sciences, France.

@author Benoit Messiaen, Matthieu Peregrini, Witold Podlejski


