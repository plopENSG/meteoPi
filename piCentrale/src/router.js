import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Dashboard from './views/Dashboard.vue'
import History from './views/History.vue'
import Compare from './views/Compare.vue'
import Map from './views/Map.vue'


Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Dashboard
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard
    },
    {
      path: '/history',
      name: 'history',
      component: History
    },
    {
      path: '/compare',
      name: 'compare',
      component: Compare
    },
    {
      path: '/map',
      name: 'map',
      component: Map
    }
  ]
})
