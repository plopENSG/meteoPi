import Vue from 'vue'
import Vuex from 'vuex'
import BootstrapVue from 'bootstrap-vue'

Vue.use(Vuex);
Vue.use(BootstrapVue);

const store = new Vuex.Store({
    state: {
        currentSondeNameDashboard: "SONDE BMW",
        currentSondeNameHistory: "SONDE BMW",
        sondes : [
            {
                id: "009",
                name: "SONDE MCM",
                ip: "172.31.43.62:3001",
                // ip: "https://0ffbc12d-e110-4ae4-8610-74ca069aaff9.mock.pstmn.io",
                measurements: {},
                location: {},
                data: []
            },
            {
                id: "010",
                name: "SONDE YSH",
                ip: "172.31.43.63:3001",
                // ip: "https://1c9eceb0-85c0-49af-a2dd-4945eb550593.mock.pstmn.io",
                measurements: {},
                location: {},
                data: []
            },
            {
                id: "011",
                name: "SONDE BMW",
                ip: "172.31.43.64:3001",
                // ip: "https://0ffbc12d-e110-4ae4-8610-74ca069aaff9.mock.pstmn.io",
                measurements: {},
                location: {},
                data: []
            }
        ],
        measures: {
            "press": {unit: "hPa", fullname: "Pressure"},
            "temp": {unit: "°C", fullname: "Temperature"},
            "hygro": {unit: "%", fullname: "Hygrometry"},
            "lum": {unit: "lux", fullname: "Luminosity"},
            "wind_dir": {unit: "°", fullname: "Wind direction"},
            "wind_mean": {unit: "knots", fullname: "Wind force mean"},
            "wind_min": {unit: "knots", fullname: "Wind force minimum"},
            "wind_max": {unit: "knots", fullname: "Wind force maximum"},
            "location": {unit: "", fullname: "Location"},
            "rain": {unit: "mm", fullname: "Rain"}
        }
    },
    mutations: {
        setDataLast(state, {result, sonde}) {
            sonde.measurements = Object.assign({}, sonde.measurements, result.measurements);
            sonde.location = Object.assign({}, result.location);
            if (result.rain !== undefined) sonde.measurements.rain = result.rain;
        },
        setDataHistory(state, {result, sonde}) {
            let c = 0;
            console.log(sonde.name, result);
            sonde.data = [];
            result.data.map((d) => {
                if (c % 15 === 0) sonde.data.push(d);
                c++;
            });
            sonde.location = Object.assign({}, result.data[result.data.length-1].location);
        }
    },
    actions: {
        requestDashboard (state, {sonde_name}) {

            let sonde = this.state.sondes.filter(sonde => sonde.name === sonde_name)[0];

            fetch("http://" + sonde.ip + "/last?capteur_type=all")
                .then(result => result.json())
                .then(result => {
                    //console.log(result);
                    this.commit('setDataLast', { result, sonde})
                })
                .catch(err => console.error(err))
        },
        requestHistory(state, {sonde_name, duration}) {

            let sonde = this.state.sondes.filter(sonde => sonde.name === sonde_name)[0];

            let dateEnd = parseInt(new Date().getTime() / 1000);
            let dateStart = null;

            let one_day = 86400;

            switch (duration) {
                case "day":
                dateStart = dateEnd - one_day;
                break;
                case "month":
                dateStart = dateEnd - 31*one_day;
                break;
                case "year":
                dateStart = dateEnd - 365*one_day;
            }

            let req_url = "http://" + sonde.ip + "/period?capteur_type=all&dateStart=" + dateStart+ "&dateEnd=" + dateEnd;

            fetch(req_url)
            .then(result => result.json())
            .then(result => {
            // console.log(result);
            this.commit('setDataHistory', { result, sonde})
            })
            .catch(err => console.error(err))

        },
        requestCompare (state, {data, isInstant, startDate, endDate}) {

            for (let i in this.state.sondes) {
                let sonde = this.state.sondes[i];

                if (isInstant) {

                    let url = new URL("http://" + sonde.ip + "/last"),
                        params = {capteur_type: data};

                    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

                    fetch(url)
                        .then(result => result.json())
                        .then(result => this.commit('setDataLast', { result, sonde}))
                        .catch(err => console.error(err));
                }
                else {
                    let url = new URL("http://" + sonde.ip + "/period"),
                        params = {capteur_type: data, dateStart: startDate, dateEnd: endDate};

                    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

                    fetch(url)
                        .then(result => result.json())
                        .then(result => this.commit('setDataHistory', { result, sonde}))
                        .catch(err => console.error(err));
                }
            }
        },
        requestMap() {
            for (let i in this.state.sondes) {
                let sonde = this.state.sondes[i];

                let url = new URL("http://" + sonde.ip + "/last"),
                    params = {capteur_type: "location"};

                Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

                fetch(url)
                    .then(result => result.json())
                    .then(result => this.commit('setDataLast', { result, sonde}))
                    .catch(err => console.error(err));
            }
        }
    }
});

export default store;
