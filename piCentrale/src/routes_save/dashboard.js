var express = require('express');
var router = express.Router();

const fetch = require('node-fetch');
const sondes_ip = require('../js/sondes');

/* GET home page. */
router.get('/', function(req, res, next) {

    let sonde_id = req.query.sonde_id;

    if (sonde_id === undefined) {
        res.json({});
        return;
    }

    //let req_url = "http://" + sondes_ip[sonde_id] + "/last?capteur_type=all";
    let req_url = "http://localhost:3001/last?capteur_type=all";

    fetch(req_url)
        .then(result => result.json())
        .then(result => res.json(result))
        .catch(err => res.render('error', {
            message: "bad response from " + req_url,
            error: err
        }));
});

module.exports = router;
