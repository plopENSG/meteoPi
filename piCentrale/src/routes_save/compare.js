var express = require('express');
var router = express.Router();

const fetch = require('node-fetch');
const sondes_ip = require('../js/sondes');


router.get('/', function(req, res, next) {

    let data = req.query.data;
    let isInstant = req.query.instant === "true";
    let dateStart = req.query.dateStart;
    let dateEnd = req.query.dateEnd;

    if (data === undefined) {
        res.json({});
        return;
    }
    if (dateStart > dateEnd) {
        res.render('error', {
            message: "Date Format not allowed",
            error: {
                status: "DateError",
                stack: "dateStart > dateEnd : " + dateStart + ", " + dateEnd
            }
        })
    }

    let query = null;

    if (isInstant) {
        query = "last?capteur_type=" + data;
    }
    else {
        query = "period?capteur_type=" + data + "&dateStart=" + dateStart + "&dateEnd=" + dateEnd;
    }

    // let req_url = "http://localhost:3001/" + req;
    //
    // fetch(req_url)
    //     .then(result => result.json())
    //     .then(result => res.json(result))
    //     .catch(err => res.render('error', {
    //         message: "bad response from " + req_url,
    //         error: err
    //     }));

    let requests = [];

    for (let i = 0; i < sondes_ip.length; i++) {

        let req_url = "http://" + sondes_ip[i] + "/" + query;

        requests.push(
            fetch(req_url)
                .then(result => result.json())
        )
    }

    Promise.all(requests)
        .then(requests => {

            let output = {};
            for (let i = 0; i < requests.length; i++) {
                output[i] = requests[i];
            }
            res.json(output);
        })
        .catch(err => res.render('error', {
            message: "bad response from sonde",
            error: err
        }));

});

module.exports = router;
