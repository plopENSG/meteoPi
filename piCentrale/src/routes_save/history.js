var express = require('express');
var router = express.Router();

const fetch = require('node-fetch');
const sondes_ip = require('../js/sondes');

/* GET home page. */
router.get('/', function(req, res, next) {

    let sonde_id = req.query.sonde_id;
    let duration = req.query.duration;

    if (sonde_id === undefined || duration === undefined) {
        res.json({});
        return;
    }

    let dateEnd = new Date().getTime();
    let dateStart = null;

    let one_day = 86400000;

    switch (duration) {
        case "day":
            dateStart = dateEnd - one_day;
            break;
        case "month":
            dateStart = dateEnd - 31*one_day;
            break;
        case "year":
            dateStart = dateEnd - 365*one_day;
    }

    //let req_url = "http://" + sondes_ip[sonde_id] + "/last?capteur_type=all&dateStart=" + dateStart + "&dateEnd=" + dateEnd;
    let req_url = "http://127.0.0.1:3001/period?capteur_type=all&dateStart=" + dateStart + "&dateEnd=" + dateEnd;

    fetch(req_url)
        .then(result => result.json())
        .then(result => res.json(result))
        .catch(err => res.render('error', {
            message: "bad response from " + req_url,
            error: err
        }));

});

module.exports = router;
