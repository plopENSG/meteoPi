var express = require('express');
var router = express.Router();

const fetch = require('node-fetch');
const sondes_ip = require('../js/sondes');


router.get('/', function(req, res, next) {

    let query = "last?capteur_type=location";

    // let req_url = "http://localhost:3001/" + query;
    //
    // fetch(req_url)
    //     .then(result => result.json())
    //     .then(result => res.json(result))
    //     .catch(err => res.render('error', {
    //         message: "bad response from " + req_url,
    //         error: err
    //     }));

    let requests = [];

    for (let i = 0; i < sondes_ip.length; i++) {

        let req_url = "http://" + sondes_ip[i] + "/" + query;

        requests.push(
            fetch(req_url)
                .then(result => result.json())
        )
    }

    Promise.all(requests)
        .then(requests => {

            let output = {};
            for (let i = 0; i < requests.length; i++) {
                output[i] = requests[i];
            }
            res.json(output);
        })
        .catch(err => res.render('error', {
            message: "bad response from " + req_url,
            error: err
        }));

});

module.exports = router;
